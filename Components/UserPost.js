import React, {useState} from 'react';
import {Image, StyleSheet, Text, View, TouchableHighlight} from 'react-native';
import {getUsername} from '../Data/Data';
import Comments from './Comments';
import {Entypo, AntDesign} from '@expo/vector-icons';
import ActivityVideo from './ActivityVideo';
import PostLoves from './PostLoves';

import { useNavigation } from '@react-navigation/native';

export default function UserPost({post, deletePost, setComments}) {
    const [loved, setLoved] = useState(post.has_loved);
    const [numLoves, setNumLoves] = useState(post.love_count);
     
    const navigation = useNavigation();

    const createPost = (numLoves, loved) => {
        return {
            id: post.id,
            post_text: post.post_text,
            video_link: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
            user: post.user,
            type: 1,
            has_loved: loved,
            display_time: "2013-01-17T17:26:43.745Z",
            comment_count: post.comment_count,
            love_count: numLoves,
            love_users: post.love_users,
            post_details: {
                thread: post.post_details.thread,
                source_feedid: "3627163162rf",
            }
        }
    }

    const userLovedPost = (hasPressed) => {
        if(hasPressed === true) {
            setLoved(!loved);
            setNumLoves(numLoves-1);
            return(
                <AntDesign 
                    name="heart" 
                    size={24} 
                    color='red' 
                />
            );
        }
        else {
            setLoved(!loved);
            setNumLoves(numLoves+1);            
            return(
                <AntDesign 
                    name="hearto" 
                    size={24} 
                />
            );
        }
    }
    //const 
    return(
        <View style={styles.contain}>
            <View style={styles.style_post}>    
                <View style={styles.top}>
                    <View style={styles.minus_icon}>
                        <Entypo 
                            name='cross'
                            color='red'
                            size='40'
                            onPress= {() => 
                                deletePost(post.id)
                            }
                        />
                    </View> 
                    <View style={styles.style_user}>
                        <View style={styles.same_row}>
                            <AntDesign name="user" size={24} color="black" />
                            <Image source={{uri: post.user.avatar_picture_urls[0].s}} />
                            <Text style={{marginLeft: 10},{fontWeight:'bold'} }>
                                {getUsername(post.user)}{"\n"} 
                            </Text> 
                        </View>
                    </View>
                </View>      
                <View style={styles.post_body}>
                    <Text style={{fontWeight:'bold'}}>Post Content: {"\n"}{"\n"}</Text>
                    <Text style={{fontFamily: 'Futura'}}>{post.post_text}{"\n"} {"\n"}</Text> 
                    <ActivityVideo videoUrl={post.video_link}/>
                    <View style={styles.same_row}>
                        <PostLoves hasLoved={loved} likeHandler={userLovedPost} numLoves={numLoves}/>

                        <TouchableHighlight 
                        style={styles.style_buttons}
                        onPress={ () => 
                            navigation.navigate('Post View', createPost(numLoves, loved))
                        }
                        >
                        <Text style={{color: 'white'}}>View Post Details</Text>
                        </TouchableHighlight>
                    </View>
                </View>   
                   
            </View>   
            {/* <Comments thread={post.post_details.thread} /> */}
            <Text style={styles.style_comment}>Comments ({post.comment_count})</Text> 
        </View>
    );
}

const styles = StyleSheet.create({
    contain:{
        backgroundColor: '#efa685',
        alignItems: 'center',
        fontFamily: 'Cochin',
        paddingBottom: 25,
        marginTop: 20,
    },
    top: {
        borderColor: 'white',
        borderWidth: 0.5,
        backgroundColor: 'white',
        borderStyle: 'dashed',
    },
    post_body: {
        marginTop: 10,
        marginBottom: 5,
        borderColor: 'white',
        borderWidth: 0.5,
        backgroundColor: 'white',
        borderStyle: 'dashed',
    },
    style_post: {
            borderColor: 'black',
            fontFamily: 'Damascus',
            borderWidth: 3,
            padding: 5,
            width: 400,
            backgroundColor: 'lightgray',
      },
      style_user: {
        width: 330,
        backgroundColor: 'white',
        marginBottom:10,
  },
      title: {
          textAlign: 'center',
          fontWeight: 'bold',
      },
      style_comment: {
        backgroundColor:'white',
        borderColor: '#bbb',
        borderWidth: 3,
        padding: 10,
        width: 300,
        textAlign: 'center',
        fontWeight: 'bold',  
      },
      align_modal: {
          marginLeft: 35,
      },
      minus_icon: {
          marginLeft: 370,
          marginBottom: 10,
      },
      same_row: {
          flex: 1,
          flexDirection: 'row',
      },
      style_buttons: {
        borderColor: 'black',
        borderStyke: 'dashed',
        borderWidth: 2,
        padding: 5,
        width: 150,
        backgroundColor: 'gray',
        textAlign: 'center',
        color: 'white',
    },
    style_comment: {
        backgroundColor:'white',
        borderColor: '#bbb',
        borderWidth: 3,
        padding: 10,
        width: 400,
        textAlign: 'center',
        fontWeight: 'bold',  
      },
});
