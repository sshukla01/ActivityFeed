import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function Title(props) {
    return(
        <View style={styles.title_background}>
            <Text style={styles.feed_title}> {props.title}</Text>
        </View>

    ); 
}

const styles = StyleSheet.create({
    feed_title: {
        textAlign: 'center',
        fontSize: 35,
        fontWeight: 'bold',
        fontFamily: 'Cochin',
        color: 'black'
      },
      comment_title: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: 'Cochin',
        color: 'black',
      },
      title_background: {
        height: 50,
        backgroundColor: 'lightgray',
      },
});


