import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AntDesign} from '@expo/vector-icons';

export default function PostLoves({hasLoved, likeHandler, numLoves}) {
    if(hasLoved === true) {
        return(
            <View style={styles.same_row}>
                <AntDesign 
                    name="heart" 
                    size={24} 
                    color='red' 
                    onPress = {() =>
                        likeHandler(hasLoved)
                    }
                />
                <Text style={styles.num_text}>{numLoves}</Text>
            </View>
        );
    }
    else {
        return(
            <View style={styles.same_row}>
                <AntDesign 
                    name="hearto" 
                    size={24} 
                    color='red' 
                    onPress = {() =>
                        likeHandler(hasLoved)
                    }
                />
                <Text style={styles.num_text}>{numLoves}</Text>
            </View>
                
        );
    }
}

const styles = StyleSheet.create({
    same_row: {
        flex: 1,
        flexDirection: 'row',
    },
    num_text: {
        marginLeft: 5,
        marginTop: 3,
        fontFamily: 'Futura',
        fontWeight: 'bold',
    },
});
