import React, { useState } from 'react';
import {TextInput, StyleSheet, View, Button} from 'react-native';

export default function NewUserPost({submitButton}) {
    const [username, setUsername] = useState('');
    const[postText, setPostText] = useState('');

    const replaceUsername = (input) => {
        setUsername(input);
    }

    const replacePostText = (input) => {
        setPostText(input);
    }

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.post_box} 
                placeholder='Enter username here'
                onChangeText={replaceUsername}
            
            />
            <TextInput 
                style={styles.post_box}
                multiline
                placeholder='Enter text here'
                onChangeText={replacePostText}
            
            />
            <Button 
                onPress={ ()=>
                    submitButton(username, postText)               
                }
                title='Create Post'
                color='gray'
            />
        </View>
    );
}

const styles = StyleSheet.create({
    post_box: {
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        margin: 15,
        width: 250,
        alignItems: 'center',
        color: 'black',
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 3,
        padding: 10,
        margin: 15,
        width: 400,
    },
});
