import React, {useState} from 'react';
import { StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import Modal from 'modal-react-native-web';
import NewComment from'./NewComment';


export default function CommentsModal({submitCommentPasser}) {
    const [isModalVisible, setModalVisible] = useState(false);

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };

    return (
        <View style={styles.container} >
            <View style={styles.modal}>
                <Modal
                ariaHideApp={false}
                animationType="fade"
                transparent={true}
                visible={isModalVisible}
                onDismiss={() => {
                    console.log("Modal closed");
                }}>
                    <View style={styles.modal}>
                        <NewComment submitComment ={submitCommentPasser}/>
                        <TouchableHighlight
                        style={styles.close_modal}
                        onPress={() => {
                            toggleModal();
                        }}>
                            <Text style={{color:'white'}}>Close</Text>
                        </TouchableHighlight>        
                    </View> 
                </Modal>
            </View>
            <TouchableHighlight
            style={styles.style_buttons}
            onPress={() => {
                toggleModal();
            }}>
                <Text style={{color:'white'}}>Add Comment</Text>   
            </TouchableHighlight>
        </View>
    );    
 }
   
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    style_buttons: {
        borderColor: 'black',
        borderStyke: 'dashed',
        borderWidth: 2,
        padding: 5,
        width: 110,
        backgroundColor: 'gray',
        textAlign: 'center',
    },
    close_modal: {
        borderColor: 'black',
        borderStyke: 'dashed',
        borderWidth: 2,
        padding: 5,
        marginTop: 2,
        width: 110,
        backgroundColor: 'gray',
        textAlign: 'center',
        width: 400,
    },
    modal_content: {
        marginTop: 22,
        backgroundColor: 'white',
    },
    modal: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
