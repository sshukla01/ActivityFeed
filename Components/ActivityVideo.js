import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Video } from 'expo-av';

export default function ActivityVideo({videoUrl}) {
    const video = React.useRef(null);
    const [status, setStatus] = React.useState({});
    return (
        <View style={styles.container}>
            <Video
                ref={video}
                style={styles.video}
                source={{
                    uri: videoUrl,
                }}
                useNativeControls
                resizeMode="contain"
                isLooping
                onPlaybackStatusUpdate={status => setStatus(() => status)}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      backgroundColor: 'white',
      
    },
    video: {
      alignSelf: 'center',
      marginTop: 20,
      width: 330,
      height: 230,
      
    },
    buttons: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
});


