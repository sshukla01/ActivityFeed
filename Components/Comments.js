import React, {useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import { createUser, getUsername } from '../Data/Data';
import CommentsModal from './CommentsModal';

export default function Comments(thread) { //how it works: Post->Thread->[Comments]->Comment  
    const[commentsArr, setCommentsArr] = useState(thread.thread.comments);
    const [numComments, setNumComments] = useState(thread.thread.comments.length)

    const onSubmitComment = (username, commentText) => {
      setCommentsArr((prevCommentsArr) => {
        setNumComments(numComments+1);
        return [{
          id: Math.random().toString(),
          user: createUser(username),
          markup_text: commentText,
          love_count: 0,
          create_time:"2021-05-31T21:35:56.609Z",
          has_loved: false,
        },
        ...prevCommentsArr
      ];
      })
    }

    // const retNumComments = (comments) => {
    //   return numComments;
    // }

    return(
      <View style={styles.container}>
        <Text style={styles.style_comment}>Comments ({numComments})</Text> 
        <FlatList
        data={commentsArr} 
        renderItem={({item}) => {
          return(
            <View style={styles.comment_box}>
              <Text style={{fontWeight:'bold'}}>{getUsername(item.user)} replied: {"\n"} </Text>
              <Text style={{fontFamily:'Futura'}}>{item.markup_text}</Text>
            </View>
          )}
        }
        />
        <CommentsModal submitCommentPasser={onSubmitComment}/>
      </View>
    );
    
}

const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
    },
    comment_box: {
      backgroundColor:'white',
      borderColor: '#bbb',
      borderWidth: 3,
      padding: 10,
      width: 400,
    },
    text_buffer: {
      marginBottom: 30,
    },
    style_comment: {
      backgroundColor:'white',
      borderColor: '#bbb',
      borderWidth: 3,
      padding: 10,
      width: 400,
      textAlign: 'center',
      fontWeight: 'bold',  
    },
    align_modal: {
      marginLeft: 35,
    },
  });
