import React, {useState} from 'react';
import {TextInput, StyleSheet, Text, View, Button} from 'react-native';

export default function NewComment({submitComment}) {
    const [username, setUsername] = useState('');
    const[commentsText, setCommentsText] = useState('');

    const handleUsername = (input) => {
        setUsername(input);
    }

    const handleComment = (input) => {
        setCommentsText(input);
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Enter Your Comment!</Text>
            <TextInput
                style={styles.post_box} 
                placeholder='Enter username here'
                onChangeText={handleUsername}
            
            />
            <TextInput 
                style={styles.post_box}
                placeholder='Enter comment text here'
                multiline
                onChangeText={handleComment}   
            />
            <View style={{marginTop:28}}>
                <Button 
                onPress={ () =>
                    submitComment(username, commentsText)     
                }
                title='Submit'
                color='gray'
                />
            </View>
            
        </View>
    );
}

const styles = StyleSheet.create({
    post_box: {
        borderColor: 'black',
        borderWidth: 3,
        padding: 10,
        margin: 15,
        width: 250,
    },
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 2,
        height: 250,
        width: 400,
    },
    title: {
        alignItems: 'center',
        fontFamily: 'Cochin',
        fontWeight: 'bold',
        textDecorationLine: 'underline',
        fontSize: 20,
        marginTop: 10,
    }
});
