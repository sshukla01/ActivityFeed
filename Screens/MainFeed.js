import React, {useState} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import {usersData, postsData, feedData, createUser, createThread} from "../Data/Data";
import UserPost from "../Components/UserPost";
import Title from "../Components/Title";
import NewUserPost from "../Components/NewUserPost";

export default function MainFeed({navigation}) {
  const [postsArr, setPostsArr] = useState(postsData);
  const [comments, setComments] = useState('');
  
  const updateComments = (numComments) => {
    setComments(numComments);
    return comments;
  }

  const onSubmitNewPost = (username, posttext) => {
    setPostsArr((prevPostsArr) => {
      return [{
        id: Math.random().toString(),
        post_text: posttext,
        video_link: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        user: createUser(username),
        type: 1,
        has_loved: false,
        display_time: "2013-01-17T17:26:43.745Z",
        comment_count: 0,
        love_count: 0,
        love_users: [usersData[1]],
        post_details: {
          thread: createThread(),
          source_feedid: "3627163162rf",
        },
      },
      ...prevPostsArr
    ];
    })
  }

  const onDeletePost = (id) => {
    setPostsArr((prevPostsArr) => {
      return prevPostsArr.filter(post => post.id != id);
      });
  }

  return(
    <View style={styles.container}>
      <Title title={feedData[0].object_name}/>
        <View style={{backgroundColor:'lightgray'}, {alignItems:'center'}}>
          <NewUserPost submitButton={onSubmitNewPost}/>
          <FlatList
          data = {postsArr}
          renderItem = {({ item }) =>  {
            return (
              <UserPost post={item} deletePost={onDeletePost} setComments={updateComments}/>
            )}
          }
          />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor:'#efa685',
    justifyContent: 'center',
    fontFamily: 'Cochin',
  },
  formatter: {
    textAlign: 'center',
    marginVertical: 15,
    color: '#FFFFFF',
  },
  formatterButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buffer: {
    marginVertical : 70,
  },
});

