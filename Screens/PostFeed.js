import React, {useState} from 'react';
import {usersData, postsData, feedData, createUser, createThread} from "../Data/Data";
import UserPost from "../Components/UserPost";
import Title from "../Components/Title";
import NewUserPost from "../Components/NewUserPost";
import { useNavigation } from '@react-navigation/native';
import {Image, StyleSheet, Text, View, TouchableHighlight} from 'react-native';
import {getUsername} from '../Data/Data';
import Comments from '../Components/Comments';
import {Entypo, AntDesign} from '@expo/vector-icons';
import ActivityVideo from '../Components/ActivityVideo';
import PostLoves from '../Components/PostLoves';



export default function PostFeed({route}) {
    //const navigation = useNavigation();
    //console.log("Route.PARAMS:",route.params);
    const post = route.params;
    const [loved, setLoved] = useState(post.has_loved);
    const [numLoves, setNumLoves] = useState(post.love_count);
    const navigation = useNavigation();

    const userLovedPost = (hasPressed) => {
        if(hasPressed === true) {
            setLoved(!loved);
            setNumLoves(numLoves-1);
            console.log("pressed", hasPressed);
            return(
                <AntDesign 
                    name="heart" 
                    size={24} 
                    color='red' 
                    onPress = {() =>
                        post.comment_count++
                    }
                />
            );
        }
        else {
            setLoved(!loved);
            setNumLoves(numLoves+1);            
            return(
                <AntDesign 
                    name="hearto" 
                    size={24} 
                />
            );
        }
    }

    return(
        <View style={styles.contain}>
            <View style={styles.style_post}>    
                <View style={styles.top}>
                    <View style={styles.style_user}>
                        <View style={styles.same_row}>
                            <AntDesign name="user" size={24} color="black" />
                            <Text style={{marginLeft: 10},{fontWeight:'bold'} }>
                                {getUsername(post.user)}{"\n"} 
                            </Text> 
                        </View>
                    </View>
                </View>      
                <View style={styles.post_body}>
                    <Text style={{fontWeight:'bold'}}>Post Content: {"\n"}{"\n"}</Text>
                    <Text style={{fontFamily: 'Futura'}}>{post.post_text}{"\n"} {"\n"}</Text> 
                    <ActivityVideo videoUrl={post.video_link}/>
                    <View style={styles.same_row}>
                        <PostLoves hasLoved={loved} likeHandler={userLovedPost} numLoves={numLoves}/>

                        <TouchableHighlight 
                        style={styles.style_buttons}
                        onPress={ () => 
                            navigation.goBack()
                        }
                        >
                        <Text style={{color: 'white'}}>Go Back</Text>
                        </TouchableHighlight>
                    </View>
                </View>      
            </View>   
            <Comments thread={post.post_details.thread} />
         </View>
    );
  }

  const styles = StyleSheet.create({
    contain:{
        backgroundColor: '#efa685',
        alignItems: 'center',
        fontFamily: 'Cochin',
        paddingBottom: 25,
        flex: 1,
    },
    style_buttons: {
        borderColor: 'black',
        borderStyke: 'dashed',
        borderWidth: 2,
        padding: 5,
        width: 150,
        backgroundColor: 'gray',
        textAlign: 'center',
        color: 'white',
    },
    top: {
        borderColor: 'white',
        borderWidth: 0.5,
        backgroundColor: 'white',
        borderStyle: 'dashed',
    },
    post_body: {
        marginTop: 10,
        marginBottom: 5,
        borderColor: 'white',
        borderWidth: 0.5,
        backgroundColor: 'white',
        borderStyle: 'dashed',
    },
    style_post: {
            borderColor: 'black',
            fontFamily: 'Damascus',
            borderWidth: 3,
            padding: 5,
            width: 400,
            backgroundColor: 'lightgray',
      },
      style_user: {
        width: 330,
        backgroundColor: 'white',
        marginBottom:10,
  },
      title: {
          textAlign: 'center',
          fontWeight: 'bold',
      },
      style_comment: {
        backgroundColor:'white',
        borderColor: '#bbb',
        borderWidth: 3,
        padding: 10,
        width: 300,
        textAlign: 'center',
        fontWeight: 'bold',  
      },
      align_modal: {
          marginLeft: 35,
      },
      minus_icon: {
          marginLeft: 370,
          marginBottom: 10,
      },
      same_row: {
          flex: 1,
          flexDirection: 'row',
      },
      style_buttons: {
        borderColor: 'black',
        borderStyke: 'dashed',
        borderWidth: 2,
        padding: 5,
        width: 110,
        backgroundColor: 'gray',
        textAlign: 'center',
    },
});
