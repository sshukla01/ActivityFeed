import MainFeed from './Screens/MainFeed';
import PostFeed from './Screens/PostFeed';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Main Feed"
      >
        <Stack.Screen name="Main Feed" component={MainFeed} />
        <Stack.Screen name="Post View" component={PostFeed} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

