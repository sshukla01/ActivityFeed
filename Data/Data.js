//Data Management Functions //////////////////

export const createUser = (username) => {
    return {
            id: Math.random().toString(),
            first_name: "Mock user",
            avatar_picture_urls: [{
                s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
                l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
            }],
            follower_count: 1, 
            has_followed: false, 
            is_expert: false,
            join_date: "2018-08-11T21:16:47.585Z",
            last_name: "Mock user",
            username: username,
            voucher_campaignid: "5733d02f3f3e5a6c72dc4013", 
            wellness_level: 41,
            wellness_minutes: 19329,
            wellness_progress: 0.13415004706092049, 
    };
}

export const createThread = () => {
    return {
        id: Math.random().toString(),
        comment_count: 0,
        comments: [],
    };
}

export const getUsername = (user) => {
    return user.username;
}

export const testComment = () => {
    console.log(threadsData[0]);
}

//Data exported [See data format below]//////////////////

export const usersData = [
    {
        id: "42b6f523f3f23ad6ab8b8cb63", 
        first_name: "Sarthak",
        avatar_picture_urls: [{
            s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
            l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
        }],
        follower_count: 1, 
        has_followed: false, 
        is_expert: false,
        join_date: "2018-08-11T21:16:47.585Z",
        last_name: "Shukla",
        username: "Sarthak Shukla",
        voucher_campaignid: "5733d02f3f3e5a6c72dc4013", 
        wellness_level: 41,
        wellness_minutes: 19329,
        wellness_progress: 0.13415004706092049, 
    },
    {
        id: "555b6f523f3f23ad6ab8b8de12", 
        first_name: "Jeff",
        avatar_picture_urls: [{
            s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
            l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
        }],
        follower_count: 1, 
        has_followed: false, 
        is_expert: false,
        join_date: "2018-08-11T21:16:47.585Z",
        last_name: "Smith",
        username: "Jeff Smith",
        voucher_campaignid: "5733d02f3f3e5a6c72dc4013", 
        wellness_level: 41,
        wellness_minutes: 19329,
        wellness_progress: 0.13415004706092049, 
    },
    {
        id: "64b6f523f3f23ad6ab8b8de12", 
        first_name: "Harry",
        avatar_picture_urls: [{
            s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
            l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
        }],
        follower_count: 1, 
        has_followed: false, 
        is_expert: false,
        join_date: "2018-08-11T21:16:47.585Z",
        last_name: "Potter",
        username: "Harry Potter",
        voucher_campaignid: "5733d02f3f3e5a6c72dc4013", 
        wellness_level: 41,
        wellness_minutes: 19329,
        wellness_progress: 0.13415004706092049, 
    },
    {
        id: "73b6f523f3f23ad6ab8b8cb63", 
        first_name: "Sheila",
        avatar_picture_urls: [{
            s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
            l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
        }],
        follower_count: 1, 
        has_followed: false, 
        is_expert: false,
        join_date: "2018-08-11T21:16:47.585Z",
        last_name: "Laskly",
        username: "Sheila Laskly",
        voucher_campaignid: "5733d02f3f3e5a6c72dc4013", 
        wellness_level: 41,
        wellness_minutes: 19329,
        wellness_progress: 0.13415004706092049, 
    },
    {
        id: "816f523f3f23ad6ab8b8cb63", 
        first_name: "Gary",
        avatar_picture_urls: [{
            s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
            l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
        }],
        follower_count: 1, 
        has_followed: false, 
        is_expert: false,
        join_date: "2018-08-11T21:16:47.585Z",
        last_name: "Ranger",
        username: "Gary Ranger",
        voucher_campaignid: "5733d02f3f3e5a6c72dc4013", 
        wellness_level: 41,
        wellness_minutes: 19329,
        wellness_progress: 0.13415004706092049, 
    },
    {
        id: "9b6f523f3f23ad6ab8b8cb63", 
        first_name: "Coral",
        avatar_picture_urls: [{
            s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
            l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
        }],
        follower_count: 1, 
        has_followed: false, 
        is_expert: false,
        join_date: "2018-08-11T21:16:47.585Z",
        last_name: "King",
        username: "Coral King",
        voucher_campaignid: "5733d02f3f3e5a6c72dc4013", 
        wellness_level: 41,
        wellness_minutes: 19329,
        wellness_progress: 0.13415004706092049, 
    },
    {
        id: "10b6f523f3f23ad6ab8b8cb63", 
        first_name: "Stuart",
        avatar_picture_urls: [{
            s: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8',
            l: 'https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8'
        }],
        follower_count: 1, 
        has_followed: false, 
        is_expert: false,
        join_date: "2018-08-11T21:16:47.585Z",
        last_name: "Little",
        username: "Stuart Little",
        voucher_campaignid: "5733d02f3f3e5a6c72dc4013",
        wellness_level: 41,
        wellness_minutes: 19329,
        wellness_progress: 0.13415004706092049, 
    },
]



export const commentsData = [
    {
        id: "60b556bce33d7352dcec92ac",
        user: usersData[2],
        markup_text: "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
    {
        id: "60b556bce33d72dcec92ac",
        user: usersData[1],
        markup_text: "Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorumby Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
    {
        id: "60b556bce33d7352dcec9d",
        user: usersData[0],
        markup_text: "It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },///////////////////
    {
        id: "60b556bce33d7352dcec9e",
        user: usersData[3],
        markup_text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
    {
        id: "60b556bce33d7352dcec9f",
        user: usersData[4],
        markup_text: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
    {
        id: "60b556bce33d7352dcec9g",
        user: usersData[5],
        markup_text: "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
    {
        id: "60b556bce33d7352dcec9h",
        user: usersData[6],
        markup_text: "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
    {
        id: "60b556bce33d7352dcec9j",
        user: usersData[7],
        markup_text: "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
    {
        id: "60b556bce33d7352dcec9l",
        user: usersData[1],
        markup_text: "Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.",
        love_count: 1,
        create_time: "2021-05-31T21:35:56.609Z",
        has_loved: false
    },
]

export const threadsData = [ 
    {
        id: "60b4c83e692e4e7df420832e",
        comment_count: 3,
        comments: [commentsData[0], commentsData[1]]
    },
    {
        id: "60b4c83e692e4e7df4202e",
        comment_count: 3,
        comments: [commentsData[2], commentsData[3], commentsData[4]]
    },
    {
        id: "60b4c83e692e4df420832e",
        comment_count: 3,
        comments: [commentsData[5]]
    },
]

export const postsData = [
    {
        id: "59125b13eddc5f77c0a46447",
        post_text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        video_link: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        user: usersData[0],
        type: 1,
        has_loved: false,
        display_time: "2013-01-17T17:26:43.745Z",
        comment_count: 2,
        love_count: 1,
        love_users: [usersData[1]],
        post_details: {
            thread: threadsData[0],
            source_feedid: "58810344a8a2c657cb436a2e",
        },
    },
    {
        id: "59125b13eddc5f77c0a46448",
        post_text: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
        video_link: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        user: usersData[1],
        type: 1,
        has_loved: false,
        display_time: "2013-01-17T17:26:43.745Z",
        comment_count: 3,
        love_count: 5,
        love_users: [usersData[1]],
        post_details: {
            thread: threadsData[1],
            source_feedid: "58810344a8a2c657cb436a2e",
        },  
    },
    {
        id: "59125b13eddc5f77c0a46449",
        post_text: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
        video_link: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        user: usersData[2],
        type: 1,
        has_loved: false,
        display_time: "2013-01-17T17:26:43.745Z",
        comment_count: 1,
        love_count: 7,
        love_users: [usersData[1]],
        post_details: {
            thread: threadsData[2],
            source_feedid: "58810344a8a2c657cb436a2e",
        },   
    }

]



export const feedData = [
    {
        objectid: "4fdf70ca63c765d32f000008",
        object_name: 'Recent Activity',
        scored_post: [postsData[0].id, postsData[1].id, postsData[2]],
        type: 0
    }
]

/* 

                                    ----TO-DO-----

* Basic Feed:
objectid: ObjectId("4fdf70ca63c765d32f000008"), // userid
object_name: 'Recent Activity', // same string for all feeds that are Feed.TYPE_HOME
scored_posts [ObjectId("59125b13eddc5f77c0a46447"),... ], // array of postids
type: 0 // Feed.TYPE_HOME


* Basic Post:
id: ObjectId("59125b13eddc5f77c0a46447"),
user: {Basic User}, // user who created the post
type: 1, // TYPE_TEXT (others include TYPE_FEAT, TYPE_JOIN_GROUP_CHALLENGE, TYPE_ACHIEVEMENT, etc.)
has_loved: true, // has the logged in user who is requesting this data loved this post?
display_time: ISODate("2013-01-17T17:26:43.745Z"),
comment_count: 1,
love_count 4,
love_users: [{Basic User},... ] // array of users who have loved this post

    * Post Details:
    thread: {Basic Thread},
    source_feedid: ObjectId("58810344a8a2c657cb436a2e") // a post can be added to many feeds, but this is the feed where it was originally created



* Basic Comment:
id: ObjectId("60b556bce33d7352dcec92ac"), // id of comment
user: {Basic User}, // user who created comment
markup_text: "This is a comment.",
love_count: 1, // number of users who have loved this comment
create_time: ISODate("2021-05-31T21:35:56.609Z"),
has_loved: false // has the logged in user who is requesting this data loved this comment?



* Basic Thread:
id: ObjectId("60b4c83e692e4e7df420832e"), // id of thread
comment_count: 1,
comments: [{Basic Comment},... ] // array of comments in thread




* Basic User:
id: ObjectId("5b6f523f3f23ad6ab8b8cb63"), // id of user
avatar_picture_urls: [{
    s: https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8,
    l: https://grokker-pictures.freetls.fastly.net/5b/9a/picture_5b9ae0020cde610d120ff51c_8
}],
first_name: "Alex",
follower_count: 14, // how many people are following this user?
has_followed: false, // is the user requesting this data following this user?
is_expert: false,
join_date: ISODate("2018-08-11T21:16:47.585Z"),
last_name: "Simrell",
username: "Alex Simrell",
voucher_campaignid: ObjectId("5733d02f3f3e5a6c72dc4013"), // id of company this person belongs to if they get grokker through their employer
wellness_level: 41,
wellness_minutes: 19329,
wellness_progress: 0.13415004706092049, // progress towards next wellness level (for showing ring around avatar image)

*/
