# ActivityFeed
Activity feed created in react native 

Current Functionality:
- User can view posts on their activity feed
    - Each post is pulled from data and displays who posted, the post content, number of comments, and number of loves
- User can input a username and post content into text inputs and click a submit button to create a new post
    - New post will look the same as normal posts with the username being the inputted val and post content being the other inputted val
    - Each post created by user will have 0 comments and 0 loves
- User can view comments on each of the posts on their feed
    - Each comment has comment text and user who posted the comment (pulled from data)
    - User can input their username and their comment and see it displayed on feed.
- User is able to remove a post they do not wish to see off their feed
    - This removes the comments of the post as well from the feed
- Videoplayer implemented inside the posts on the feed
    - Video is pulled from post object in data
    - User can play, pause, go to certain time in video, as well as make video fullscreen
- User can see number of loves on a post next to the heart icon
    - User can click on empty heart and it will show a filled heart to show user liked the post
    - It will also increase the # of Loves to reflect user's input
- You can click on view post button on a post to see a post in a different screen
    - Post with comments will pop up
- Two screens: home page is the whole feed with just the posts showing, and each post has its own page which loads all the comments and previous functionality mentioned aboved
    - User can love a post on main feed and see the change reflected on post's page
    - User is able to add a comment and see changes reflected on post's page



In Progress:
- Update the number of comments when you add a post on post's page, and then go back to main feed page


To-Dos:
1. Fixing data transfer from post pages -> main page (popping off the stack but retaining data)
2. Drop down menu for comments (only view one at a time, and click a button to see more comments on a post)


